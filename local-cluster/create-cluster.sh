#!/usr/bin/env bash

cluster_name=${1:-sb-cluster}
echo "--== Create Kind cluster $cluster_name"

reg_name=$cluster_name'-control-plane'

running="$(docker inspect -f '{{.State.Running}}' "${reg_name}" 2>/dev/null || true)"
if [ "${running}" != 'true' ]; then
    kind create cluster --name "$cluster_name" --config config/cluster-config.yaml
else
  echo "A kind cluster is already running."
fi
