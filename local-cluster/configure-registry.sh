#!/usr/bin/env bash

echo "--== Configure registry"

# connect cluster to local registry
docker network connect "kind" "kind-registry" || true

# Document the local registry
# https://github.com/kubernetes/enhancements/tree/master/keps/sig-cluster-lifecycle/generic/1755-communicating-a-local-registry
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: local-registry-hosting
  namespace: kube-public
data:
  localRegistryHosting.v1: |
    host: "localhost:${reg_port:-5000}"
    help: "https://kind.sigs.k8s.io/docs/user/local-registry/"
EOF

skaffold config set kind-disable-load true
skaffold config set default-repo localhost:"${reg_port:-5000}"
skaffold config set insecure-registries http://localhost:"${reg_port:-5000}"
