#!/usr/bin/env bash

cluster_name=${1:-sb-cluster}

echo "--== Delete Kind cluster $cluster_name"

kind delete cluster --name "$cluster_name"
