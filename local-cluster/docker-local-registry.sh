#!/usr/bin/env bash

echo "--== Install local registry"

# create registry container unless it already exists
reg_name='kind-registry'
reg_port='5000'
running="$(docker inspect -f '{{.State.Running}}' "${reg_name}" 2>/dev/null || true)"
if [ "${running}" != 'true' ]; then
  docker run -d \
    --restart=always \
    --name "${reg_name}" \
    -p "127.0.0.1:${reg_port}:5000" \
    registry:2
else
  echo "Local docker registry is already running"
fi
