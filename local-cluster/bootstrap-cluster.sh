#!/bin/sh
cd "$(dirname "$0")" || exit 1

CLUSTER_NAME="sb-cluster"

# Refresh sub modules
cd ..
git config push.recurseSubmodules check
git submodule update --init --recursive --remote
cd - || exit 1

#create local docker registry
./docker-local-registry.sh

# create cluster
./create-cluster.sh $CLUSTER_NAME

# configure registry
./configure-registry.sh

# install MetalLb
subnet=${1:-$(curl -s ifconfig.me)}
helm upgrade --install metallb metallb \
  --repo https://metallb.github.io/metallb \
  --values config/metallb.values.yaml \
  --set "configInline.address-pools[0].addresses={${subnet}/32}"


./../common/install-cert-manager.sh
./../common/install-admiralty.sh

# Deploy umbrella chart
kubectl create ns workflow
helm upgrade irtsb sb-services              \
  --install                                 \
  --repo http://irtsb.ml/api/helm-registry/ \
  --version 0.2.0                           \
  --create-namespace \
  --values ./config/opencloud.values.yaml

#install Open Telemetry and aggregators
./../common/install-telemetry.sh

#install microservices
./../microservices/bootstrap-services.sh localhost:5000
