#!/usr/bin/env bash
cd "$(dirname "$0")" || exit 1

terraform -chdir=terraform init
terraform -chdir=terraform destroy -auto-approve -var-file=values.auto.tfvars
