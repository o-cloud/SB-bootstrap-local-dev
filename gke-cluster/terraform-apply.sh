#!/usr/bin/env bash
cd "$(dirname "$0")" || exit 1

terraform -chdir=terraform init
terraform -chdir=terraform apply -auto-approve -var-file=values.auto.tfvars "$@"

GKE_ZONE=$(grep zone terraform/values.auto.tfvars | cut -d "=" -f 2 | tr -d '[:space:]' | tr -d '"')
clusternames=$(terraform -chdir=terraform output -json discovery_cluster_name | tr -d '[]"')
externalips=$(terraform -chdir=terraform output -json lb_address_traefik | tr -d '[""]')

IFS=',' read -ra CLUST <<<"$clusternames"
IFS=',' read -ra EXTIP <<<"$externalips"
for clustername_key in "${!CLUST[@]}"; do
  clustername="${CLUST[$clustername_key]}"
  externalip="${EXTIP[$clustername_key]}"
  echo -e "\n----------Process $clustername------------"

  #get gcloud cluster credential
  echo "Fetching credentials for cluster $clustername ($GKE_ZONE)"
  gcloud container clusters get-credentials "$clustername" --region="$GKE_ZONE"

  # Deploy prerequisites
  ./../common/install-cert-manager.sh
  ./../common/install-admiralty.sh

  # Deploy umbrella chart
  kubectl create ns workflow
  helm upgrade irtsb sb-services              \
    --repo http://irtsb.ml/api/helm-registry/ \
    --version 0.2.0                           \
    --install                                 \
    --create-namespace                        \
    --values ./config/opencloud.values.yaml   \
    --set traefik.service.spec.loadBalancerIP="$externalip"


  #install Open Telemetry and aggregators
  ./../common/install-telemetry.sh

  #install prometheus
  ./../common/install-prometheus.sh

  #install microservices
  ./../microservices/bootstrap-services.sh

  echo "----------$clustername Done------------"
done
