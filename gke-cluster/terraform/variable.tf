variable "region" {
  type = string
  default = "europe-west1"
}

variable "zone" {
  type = string
  default = "europe-west1-d"
}

variable "project_id" {
  type = string
  default = ""
}

variable "cluster_name" {
  type = set(string)
}

variable "node_pool_size" {
  type = number
  default = 3
}

variable "disk_size" {
  type = number
  default = 10
}

variable "machine_type" {
  type = string
  default = "e2-small"
}
