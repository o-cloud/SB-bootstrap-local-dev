resource "google_container_cluster" "discovery-cluster" {
  for_each = var.cluster_name
  name     = each.key
  location = var.zone

  remove_default_node_pool = true
  initial_node_count = 1

  master_auth {
    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "node-pool" {
  for_each   = var.cluster_name
  name       = "${google_container_cluster.discovery-cluster[each.key].name}-node-pool"
  location   = var.zone
  cluster    = google_container_cluster.discovery-cluster[each.key].name
  node_count = var.node_pool_size

  node_config {
    preemptible  = true
    machine_type = var.machine_type
    disk_size_gb = var.disk_size

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only"
    ]
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }
}

resource "google_compute_address" "lb-static-ip-traefik" {
  for_each = var.cluster_name
  name   = "${each.key}-ipfs-lb-ip-traefik"
  region = var.region
}

output "lb_address_traefik" {
  value = [
    for ip in google_compute_address.lb-static-ip-traefik : ip.address
  ]
}

output "discovery_cluster_name" {
  value = [
    for cluster in google_container_cluster.discovery-cluster : cluster.name
  ]
}
