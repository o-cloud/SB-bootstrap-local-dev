#!/usr/bin/env bash

echo "--== Install cert-manager (Helm)"

helm upgrade cert-manager cert-manager  \
  --repo https://charts.jetstack.io     \
  --version v1.4.0                      \
  --install                             \
  --namespace cert-manager              \
  --create-namespace                    \
  --set installCRDs=true                \
  --wait
