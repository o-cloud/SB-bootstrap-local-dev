#!/usr/bin/env bash

echo "--== Install prometheus (Helm)"

helm upgrade prometheus kube-prometheus-stack \
  --repo https://prometheus-community.github.io/helm-charts \
  --install \
  --namespace monitoring \
  --create-namespace \
  --wait
