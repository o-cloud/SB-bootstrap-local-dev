function checkCommand() {
  if ! command -v "$1" &>/dev/null; then
    if [[ -n "$2" ]]; then
      echo "[WARN] $1 could not be found ($2)"
    else
      echo "[FAIL] $1 could not be found"
      exit 1
    fi
  else
    echo "[ OK ] $1"
  fi
}

echo "-- Check prerequisites"
checkCommand skaffold
checkCommand helm
checkCommand kind "Required for local"
checkCommand gcloud "Required for GKE"
checkCommand terraform "Required for GKE"
checkCommand jq "optional"
checkCommand kubens "optional"
checkCommand kubectx "optional"
