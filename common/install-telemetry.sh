#!/bin/sh
cd "$(dirname "$0")"


kubectl create namespace telemetry

#Waiting for cert manager
kubectl -n cert-manager wait deploy/cert-manager-webhook --for condition=available
#Wait a little more


# Install the Otel operator
helm install opentelemetry-operator opentelemetry-operator \
  --repo https://open-telemetry.github.io/opentelemetry-helm-charts \
  --version 0.1.0 \
  --wait 


#Installation zipkin
kubectl apply -f config/zipkin-deploy.yaml

#Installation jaeger
kubectl apply -f config/jaeger-all-in-one-template.yaml

#Waiting for operator
kubectl -n opentelemetry-operator-system rollout status deploy/opentelemetry-operator-controller-manager
#Déclaration des collecteurs otel (deployment, daemon et sidecar)
kubectl apply -f config/collector-otel.yaml

#Déclaration de la configmap de configuration rapide pour les pods
kubectl apply -f config/otel-configmap.yaml