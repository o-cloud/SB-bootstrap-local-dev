#!/usr/bin/env bash

echo "--== Install admiralty (Helm)"

helm upgrade admiralty multicluster-scheduler \
  --repo https://charts.admiralty.io \
  --version 0.13.2 \
  --install \
  --namespace admiralty \
  --create-namespace \
  --wait \
  --debug

kubectl label namespace admiralty application=admiralty --overwrite
