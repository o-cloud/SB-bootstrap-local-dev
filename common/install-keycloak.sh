#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 1

helm upgrade keycloak keycloak \
  --repo https://charts.bitnami.com/bitnami \
  --install \
  --values=config/keycloak-config.yaml

kubectl apply -f config/auth-configmap.yaml
