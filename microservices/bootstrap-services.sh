#!/usr/bin/env bash
cd "$(dirname "$0")"

if [ $# -eq 0 ] 
then
    repo=eu.gcr.io/irt-sb
else
    repo=$1
fi

#install microservices
echo "Launch all microservices"
skaffold run --default-repo=$repo

#add default network
echo "Add default network"
cd cluster-discovery
kubectl apply -f config/samples/cluster1_default_network_traefik.yaml
