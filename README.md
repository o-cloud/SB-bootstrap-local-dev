# Bootstrap-local-dev

## Prerequisites

* Mandatory
  * [skaffold](https://skaffold.dev/)
  * [helm](https://helm.sh/)
  * For local clusters
    * [kind](https://kind.sigs.k8s.io/)
  * For GKE clusters
    * [terraform](https://www.terraform.io/)
    * [gcloud](https://cloud.google.com/sdk/docs/install)
* Convenience tools
  * [jq](https://stedolan.github.io/jq/) 
  * [kubectx](https://github.com/ahmetb/kubectx)
  * [kubens](https://github.com/ahmetb/kubectx)

Install on Windows via Chocolatey (`https://chocolatey.org/packages/kind`)

```bash
choco install -y kind
choco install -y skaffold
choco install -y kubectx
choco install -y kubens
choco install -y kubernetes-helm
choco install -y jq
choco install -y terraform
```

Install gcloud: `https://cloud.google.com/sdk/docs/install?hl=fr`

Authentication on GCP project:

```bash
gcloud init
gcloud auth login
gcloud auth application-default login
```

Init git submodules:

```bash
git config push.recurseSubmodules check
git submodule update --init --recursive --remote
```

## Cluster installation

Choose your development cluster.  
You can set up a second cluster and a third cluster on different environment for network connection testing.  
All cluster are created by default with all microservices and default network.

### Kind cluster (local)

Install local kind cluster:
```bash
./local-cluster/bootstrap-cluster.sh
```

Delete local kind cluster:

```bash
./local-cluster/delete-cluster.sh
```

### GKE cluster (cloud)

Install gke cluster with as many name as cluster you want:

```bash
./gke-cluster/terraform-apply.sh -var='cluster_name=["discovery-cluster-1","discovery-cluster-2"]'
```

Delete gke cluster:

```bash
./gke-cluster/terraform-destroy.sh
```

## Ingress (Traefik)

### Routing

Define a service for your pod like usual

Define an IngressRoute as following 
```yaml
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: {{ .Release.Name }}-route
  namespace: {{ .Release.Namespace }}
spec:
  entryPoints:
    - entryPointName # Replace with the desiredEntrypoint
  routes:
  - match: PathPrefix(`/api/service/route`) # Replace with desired path
    kind: Rule
    services:
    - name: {{ .Release.Name }}-service
      port: 1234 # Replace with desired port number
```

### Dashboard (and monitoring)

To access Traefik Dashboard to see current routes and services available use those command 
```bash
kubens traefik-v2
kubectl port-forward $(kubectl get pods --selector "app.kubernetes.io/name=traefik" --output=name) 9000:9000
```

From the dashboard you can very that traefik picked  up your routes defined by ingress routes

## MongoDB service access

Deploy your application in the namespace `default`

Use `mongodb-service` as host to acces mongodb

Use the following yaml config for the environment variable that contain the mongo password
```yaml
- name: MONGO_PASSWORD
  valueFrom:
    secretKeyRef:
      name: mongo-mongodb
      key: mongodb-root-password
```

Or use directly mongodb connexion string

```yaml
- name: MONGO_STRING
  valueFrom:
    secretKeyRef:
      name: mongo-mongodb-connexion
      key: connexion-string
```


## Microservices launch

Microservices installation depend on your current kubectl context (use kubectx).  
Used container repository depend on the `--default-repo` option in skaffold command.

### Install all microservices

A skaffold YAML file is designed to deploy all microservices on a cluster in one command.

```bash
cd microservices
skaffold run --default-repo=eu.gcr.io/irt-sb
```

or with local registry :

```bash
cd microservices
skaffold run --default-repo=localhost:5000
```

Join the default network:

```bash
cd microservices/cluster-discovery
kubectl apply -f config/samples/cluster1_default_network_traefik.yaml
```

### Development mode

For development purpose, launch the service needed in dev mode with "skaffold dev" command.  
dev mode is designed for live reload.

Launch in dev mode with local repository (local kind only):

```bash
skaffold dev --default-repo=localhost:5000
```

Launch in dev mode with gcp repository:

```bash
skaffold dev --default-repo=eu.gcr.io/irt-sb
```
